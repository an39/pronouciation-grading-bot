recordingBlob = 0;
var textIndex = 0;
var recordButton = $('#recordingButton');
var prevButton = $('#prevButton');
var nextButton = $('#nextButton');
const answers = [
  'one',
  'two',
  'three',
  'four',
  'five'
];



async function evaluateFileFunc() {
  var form = new FormData();
  var res = false;
  form.append("text", $('#sampleText').val());
  form.append("user_audio_file", $('#recordingFile')[0].files[0]);
  form.append("question_info", "'u1/q1'");
  var settings = {
    "url": "https://api.speechace.co/api/scoring/text/v0.5/json?key=5DY%2FTK%2B6faTZHc0uRMIuQ%2Fmp%2Ba7l1hr%2FcpnopcA4G4wj%2Fy8p3KR9iVhRsnJyMViBnOplqlBZIKPkRiO17hHX%2BfxMEyc6ODImjsk8noGi9KF5Uy9I6UUSdTfApNNWdmzF&dialect=en-us&user_id=XYZ-ABC-99001",
    "method": "POST",
    "timeout": 0,
    "processData": false,
    "mimeType": "multipart/form-data",
    "contentType": false,
    "data": form,
  };
  await $.ajax(settings).done(function (response) {
    console.log(response);
    var obj = jQuery.parseJSON(response);
    if (obj.text_score.quality_score >=60)
      res = true;
  });
  console.log(res);
  return res;
}

async function evaluateRecordingFunc(){
  recordButton.prop("disabled",true);
  var form = new FormData();
  var res = false;
  form.append("text", answers[textIndex]);
  form.append("user_audio_file", recordingBlob);
  form.append("question_info", "'u1/q1'");
  var settings = {
    "url": "https://api.speechace.co/api/scoring/text/v0.5/json?key=5DY%2FTK%2B6faTZHc0uRMIuQ%2Fmp%2Ba7l1hr%2FcpnopcA4G4wj%2Fy8p3KR9iVhRsnJyMViBnOplqlBZIKPkRiO17hHX%2BfxMEyc6ODImjsk8noGi9KF5Uy9I6UUSdTfApNNWdmzF&dialect=en-us&user_id=XYZ-ABC-99001",
    "method": "POST",
    "timeout": 0,
    "processData": false,
    "mimeType": "multipart/form-data",
    "contentType": false,
    "data": form,
  };
  await $.ajax(settings).done(function (response) {
    console.log(response);
    var obj = jQuery.parseJSON(response);
    printEvaluationScore(response);
    if (obj.text_score.quality_score >=60)
      res = true;
  });
  console.log(res);
  if(res==true) {
    document.getElementById("card_body").className = "mx-auto bg-success";
    document.getElementById("respond").innerHTML = "Result is RIGHT";
  }
  else {
    document.getElementById("card_body").className = "mx-auto bg-danger";
    document.getElementById("respond").innerHTML = "Result is WRONG";
  }
  recordButton.prop("disabled",false);
  prevButton.prop("disabled",false);
  nextButton.prop("disabled",false);
  return res;
}

function printEvaluationScore(response)
{
  var obj = jQuery.parseJSON(response);
  var container =$('#resultContainer');
  container.empty();
  textScore = obj.text_score;
  container.append("Overall score: " + textScore.quality_score.toFixed(2) + "<br>");
  container.append("Details: <br>");
  wordScoreList = textScore.word_score_list;
  $.each(wordScoreList[0].phone_score_list,function (index,element){
    container.append("Phone: \"" + element.phone);
    container.append("\"\tScore: " + element.quality_score.toFixed(2));
    container.append("\tSounded like: " + element.sound_most_like);
    container.append("<br>");
  })

}

function indexIncrease() {
  if (textIndex>=4) return 4;
  textIndex += 1; 
  console.log ("Text Index: " + textIndex);
  return textIndex;
}
function indexDecrease() {
  if(textIndex<=0) return 0;
  textIndex -= 1;
  console.log ("Text Index: " + textIndex);
  return textIndex;
}

// $('#indexIncreaseButton').click(indexIncrease);
// $('#indexDecreaseButton').click(indexDecrease);
// $("#evaluateFileButton").click(evaluateFileFunc);
// $('#evaluateRecordingButton').click(evaluateRecordingFunc);
