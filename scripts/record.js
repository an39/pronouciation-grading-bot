var recorder, gumStream;
var recordButton = $('#recordingButton');
var prevButton = $('#prevButton');
var nextButton = $('#nextButton');

function toggleRecording() {
    prevButton.prop("disabled",true);
    nextButton.prop("disabled",true);
    console.log("Recording...");
    document.getElementById("card_body").className = "mx-auto";
    document.getElementById("respond").innerHTML = "Recording ... "
    if (recorder && recorder.state == "recording") {
        console.log("Stopped recording.")
        document.getElementById("respond").innerHTML = "Processing ... "
        recorder.stop();
        gumStream.getAudioTracks()[0].stop();
    } else {
        navigator.mediaDevices.getUserMedia({
            audio: true
        }).then(function(stream) {
            gumStream = stream;
            recorder = new MediaRecorder(stream);
            recorder.ondataavailable = function(e) {
                var url = URL.createObjectURL(e.data);
                recordingBlob = e.data;
                if ($('#inputAudio').length)
                {
                    $('#inputAudio').replaceWith(jQuery('<audio/>', {
                        id: 'inputAudio',
                        controls: true,
                        src: url,
                    }));
                }
                else
                {
                    global = 4;
                    jQuery('<audio/>', {
                        id: 'inputAudio',
                        controls: true,
                        src: url,
                    }).appendTo('#inputAudioContainer');
                }  
                console.log(url);
                return evaluateRecordingFunc();              
            };
            recorder.start();
        });
    }
}
// recordButton.click(toggleRecording);
